export default {
  name: 'VTable',
  props: {
    records: Array,
    columns: Array,
    groupBy: String,
  },
  computed: {
    groupHeaderIndices() {
      if (!this.groupBy) return [];

      const indices = [0];
      for(
        let i = 1, previous = this.records[0][this.groupBy], current;
        i < this.records.length;
        i++, previous = current
      ) {
        current = this.records[i][this.groupBy];
        if (current !== previous) {
          indices.push(i);
        }
      }

      return indices;
    }
  }
}

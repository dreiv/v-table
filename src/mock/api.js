import mockData from './mockData.json';

import { groupRecords } from './scripts/groupRecords';

const latency = () => Math.random() * 500 + 300; // simulate network latency of 300-800ms

export const API = {
  load: async (
		pageNumber = 1,
		pageSize = 25,
    { groupBy } = {}
  ) => new Promise(resolve => {
    const auxiliary = {};
    let records = mockData;

    if (groupBy) {
      auxiliary.groupBy = groupBy;
      records = groupRecords(records, groupBy);
    }

    const data = {
      ...auxiliary,
      totalRecords: records.length,
      pageNumber,
      pageSize,
      records
    }
    setTimeout(() => resolve(data), latency());
  })
}

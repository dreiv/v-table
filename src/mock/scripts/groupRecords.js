export const groupRecords = (records, groupBy) => {
	const groups = records.reduce((acc, record) => {
		const key = record[groupBy];
		acc[key] = (acc[key] || []).concat(record);

		return acc;
	}, {});

	const groupsEntries = Object.entries(groups);

	return groupsEntries.reduce((acc, group) => acc.concat(group[1]), []);
};
